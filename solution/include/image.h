#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H
#pragma once
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>



struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* pixel;
};



#pragma pack(push, 1)
struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};
#pragma pack(pop)

inline struct pixel get_pixel(struct image const* source, const uint64_t x, const uint64_t y){
	return source->pixel[source->width*y + x];
}
#endif
