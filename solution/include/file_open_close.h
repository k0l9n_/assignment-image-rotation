#pragma once
#include <stdbool.h>
#include <stdio.h>

bool open_file (FILE** file, char* path, char* mode);

bool close_file (FILE * const *file);

