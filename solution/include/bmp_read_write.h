#include "image.h"
#include "stdio.h"

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE = 1,
	READ_INVALID_HEADER,
    READ_PIXELS_ERROR,
    READ_PADDING_ERROR

};



enum write_status {
	WRITE_OK = 0,
    WRITE_HEADER_ERROR = 1,
    WRITE_PIXELS_ERROR,
    WRITE_PADDING_ERROR

};


enum read_status from_bmp( FILE* const in, struct image *img);
enum write_status to_bmp( FILE* out, const struct image* img);
void print_read_status(enum read_status status);
void print_write_status(enum write_status status);

