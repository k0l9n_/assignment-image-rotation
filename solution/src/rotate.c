#include "../include/rotate.h"

#include "malloc.h"

extern inline struct pixel get_pixel(struct image const* source, const uint64_t x, const uint64_t y);

struct image rotate(struct image const* source) {
	uint64_t width = source->height;
	uint64_t height = source->width;
	struct image img_output = {
		.width = width, 
		.height = height,
		.pixel = malloc(sizeof(struct pixel) * width * height)
	};


	for (size_t y = 0; y < height; y++) {
		for (size_t x = 0; x < width; x++) {
			img_output.pixel[width * y + x] = get_pixel(source,y,width-x-1);
		}
	}

	return img_output;
}
