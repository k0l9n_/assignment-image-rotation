#include "../include/file_open_close.h"
#include "../include/bmp_read_write.h"
#include "../include/rotate.h"
#include <assert.h>
#include <err.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>



int main(int args, char **argv) {

    if (args != 3) {
        fprintf(stderr,"Not enough arguments");
        return 1;
    }

    //init arguments
    char *file_input = argv[1];
    char *file_output = argv[2];


    //open input file
    FILE* file_in = NULL;
    bool is_open = open_file(&file_in, file_input, "rb");
    if (!is_open) {
	fprintf(stderr,"Problen with opening input file");
	return 2;
    }

    //read from file
    struct image image_input = {0};
    enum read_status readStatus = from_bmp(file_in, &image_input);
    if (readStatus != READ_OK) {
        free(image_input.pixel);
    	close_file(&file_in);
	    print_read_status(readStatus);
	    return 3;
    }
    
    //close input file
    bool is_close = close_file(&file_in);
    if (!is_close) {
        free(image_input.pixel);
        fprintf(stderr,"Problen with closing input file");
        return 4;
    }
     

    //rotate
    struct image img_output = rotate(&image_input);
    if (img_output.width == 0) {
    	free(image_input.pixel);
	fprintf(stderr,"Rotation problem");
	return 5;	
    }
    //free memory
    free(image_input.pixel);

    //open output file
    FILE* file_out = NULL;
    is_open = open_file(&file_out, file_output, "wb");
    if (!is_open) {
        free(img_output.pixel);
	    fprintf(stderr,"Problen with opening output file");
	return 2;
    }
    
    //write to file
    enum write_status writeStatus = to_bmp(file_out, &img_output);
    if (writeStatus!= WRITE_OK){
        free(img_output.pixel);
    	close_file(&file_out);
	    print_write_status(writeStatus);
	return 6;
    }
    
    //close output file
    is_close = close_file(&file_out);
    if (!is_close) {
        free(img_output.pixel);
        fprintf(stderr,"Problen with closing input file");
        return 4;
    }
    
    
    //free memory
    free(img_output.pixel);

    return 0;
}
