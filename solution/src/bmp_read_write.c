#include "../include/bmp_read_write.h"
#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header{
	uint16_t bfType; //0x4D42
	uint32_t bfileSize;
	uint32_t bfReserved; // 0
	uint32_t bOffBits; //often 54
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes; // 1
	uint16_t biBitCount; // 24
	uint32_t biCompression; // 0
	uint32_t biSizeImage; // often 0
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};
#pragma pack(pop)

static inline uint8_t get_padding(uint32_t width) {
	return width % 4;
}

static size_t read_header(FILE* const in, struct bmp_header* const header) {
	const size_t count = fread(header, sizeof(struct bmp_header), 1, in);
	if (count != 1) return -1;
	
	return count;
}



static enum read_status is_header_correct(struct bmp_header* const header) {
	if (header->bfType != 0x4D42) return READ_INVALID_SIGNATURE;
	if (header->bfReserved != 0) return READ_INVALID_SIGNATURE;
	if (header->biPlanes != 1) return READ_INVALID_SIGNATURE;
	if (header->biBitCount != 24) return READ_INVALID_SIGNATURE;
	if (header->biCompression != 0) return READ_INVALID_SIGNATURE;

	return READ_OK;
}



static enum read_status read_data( struct image* img_input, FILE* const in, uint32_t width, uint32_t height) {
	uint64_t count = 0;
	img_input->width = width;
	img_input->height = height;
	img_input->pixel = malloc(sizeof(struct pixel) * height * width);
	const uint8_t padding = get_padding(width);
	for (size_t i = 0; i < height; i++) {
		
		count = fread(img_input->pixel+i*width, sizeof(struct pixel), width, in);
		if (count != width) {
			free(img_input->pixel);
			return READ_PIXELS_ERROR;
		}
		if (fseek(in, padding, SEEK_CUR) != 0) {
			free(img_input->pixel);
			return READ_PADDING_ERROR;
		}

	}
	
	return READ_OK;
}

enum read_status from_bmp(FILE* const in, struct image* img) {
	//read header
	struct bmp_header header = {0};
	if (read_header(in, &header) == -1) {
		return READ_INVALID_HEADER;
	}
	
	//check header
	if (is_header_correct(&header) != READ_OK) {
		return READ_INVALID_SIGNATURE;
	}

	
	//read data
	const uint32_t width = header.biWidth;
	const uint32_t height = header.biHeight;

	return read_data(img, in, width, height);
}



static inline struct bmp_header make_header(const struct image *img) {
	const uint32_t sizeImage = img->height * img->width * sizeof(struct pixel) + get_padding(img->width) * img->height;
	struct bmp_header header = {
		.bfType = 0x4D42,
		.bfileSize = sizeof(struct bmp_header) + sizeImage,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = 40,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = sizeImage,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,

	};
	return header;
}



static enum write_status write_image(const struct image *img, FILE *out) {
	uint64_t count = 0;
	const uint64_t width = img->width;
	const uint64_t height = img->height;
	const uint64_t zero = 0;
	const uint8_t padding = get_padding(width);
	for (size_t i = 0; i < height; ++i) {
		count = fwrite(img->pixel+i* width, sizeof (struct pixel), width, out);
		if (count != width){
			return WRITE_PIXELS_ERROR;
		}
		
		if(fwrite(&zero,1,padding,out)!=padding){
			return WRITE_PADDING_ERROR;
		}
	}
	return WRITE_OK;
}




enum write_status to_bmp(FILE *out, const struct image *img) {

	//make new header
	struct bmp_header header = make_header(img);

	
	//write header
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_HEADER_ERROR;
	}

	
	//write image
	return write_image(img, out);	
}

static char* const read_status_explanations[] = {
        [READ_OK] =                     "Read ok\n",
        [READ_INVALID_SIGNATURE] =      "Error: invalid signature of BMP header\n",
        [READ_INVALID_HEADER] =         "Error: problem with reading the header\n",
        [READ_PIXELS_ERROR] =           "Error: problem with reading pixels\n",
        [READ_PADDING_ERROR] =          "Error: problem with reading padding\n"
};

void print_read_status(enum read_status status) {
	if(status!=READ_OK){
		fprintf(stderr, "%s", read_status_explanations[status]);
	}else{
		printf("%s", read_status_explanations[status]);
	}
    
}

static char* const write_status_explanations[] = {
        [WRITE_OK] =            "Write ok\n",
        [WRITE_HEADER_ERROR] =  "Error: problem with writing the header\n",
        [WRITE_PIXELS_ERROR] =  "Error: problem with writing pixels\n",
        [WRITE_PADDING_ERROR] = "Error: problem with writing paddings\n"
};

void print_write_status(enum write_status status) {
    if(status!=WRITE_OK) {
        fprintf(stderr, "%s", write_status_explanations[status]);
    }else{
		printf("%s", write_status_explanations[status]);
	}
        
}

